import 'dart:io';

bool isWin(String number) {
  switch (number) {
    case '436594':
      print("6,000,000 baht");
      return true;
    case '436595':
      print("You reward : 100,000 baht");
      return true;
    case '436593':
      print("You reward : 100,000 baht");
      return true;
    case '502412':
      print("You reward : 200,000 baht");
      return true;
    case '084971':
      print("You reward : 200,000 baht");
      return true;
    case '422058':
      print("You reward : 80,000 baht");
      return true;
    case '666926':
      print("You reward : 80,000 baht");
      return true;
    case '181516':
      print("You reward : 40,000 baht");
      return true;
    case '811661':
      print("You reward : 40,000 baht");
      return true;
    case '581874':
      print("You reward : 20,000 baht");
      return true;
    case '328183':
      print("You reward : 20,000 baht");
      return true;
  }
  return false;
}

void main() {
  String? number;
  print("Enter number : ");
  number = stdin.readLineSync()!;
  if (isWin(number)) {
    print("Congratulation $number");
  } else {
    print("Sorry..");
  }
}
