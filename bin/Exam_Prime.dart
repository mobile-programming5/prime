import 'dart:io';

bool isPrime(int num) {
  if (num == 1) return false;
  for (var i = 2; i < num; i++) {
    if (num % 2 == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  int? num;
  print("Enter number : ");
  num = int.parse(stdin.readLineSync()!);
  if (isPrime(num)) {
    print("$num is prime");
  } else {
    print("$num is not prime");
  }
}